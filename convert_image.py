from PIL import Image
from pdf2image import convert_from_path
import operator
import os
import shutil
import scipy.optimize as sco
import numpy as np
import numba as nb
from datetime import datetime

GOOD_COLOURS = [(230, 159, 0), (86, 180, 233), (0, 158, 115), (240, 228, 66), (0, 114, 178), (213, 94, 0), (204, 121, 167)]

@nb.njit
def get_colours(img_array):
    """Reads out the colours and their occurances from img, returns dict"""
    colours = {}
    for c in img_array:
        c = (c[0], c[1], c[2])
        if c in colours:
            colours[c] += 1
        else:
            colours[c] = 1
    return colours

@nb.njit
def is_grey(c):
    """checks if all of a colour's components are close enough to be grey."""
    if (c[0]-c[1])**2 + (c[1]-c[2])**2 + (c[2]-c[0])**2 <= 3*5**2:
        return True
    return False

@nb.njit
def flipped(c):
    """returns the inverted colour"""
    return (255-c[0], 255-c[1], 255-c[2])

def get_non_greys(colours):
    """returns a dictionary of only the colours that aren't grey and their #"""
    output_colours = {}
    for c in colours.keys():
        if not is_grey(c):
            output_colours[c] = colours[c]
    return output_colours

def hue(c):
    """Returns the hue of an RGB colour."""
    M = max(c[0], c[1], c[2])
    m = min(c[0], c[1], c[2])
    C = M-m
    if C==0:
        return None # this is grey -> shouldn't happen
    elif M==c[0]:
        return round(((c[1]-c[2])/C%6)*3)
    elif M==c[1]:
        return round(((c[2]-c[0])/C + 2)*3)
    elif M==c[2]:
        return round(((c[0]-c[1])/C + 4)*3)

def get_dominant_nongreys(colours):
    """returns a list of dominant non-greys by matching colours to hues."""
    dominant_colours = set()
    non_greys = get_non_greys(colours)
    if not len(non_greys)==0:
        last_value = max(non_greys.values())
        sorted_non_greys = sorted(non_greys.items(), 
                        key=operator.itemgetter(1))[::-1]
        hue_list = []
        for pair in sorted_non_greys:
            hue_list.append((hue(pair[0]), pair[1], pair[0]))
        hue_dict = dict()
        for pair in hue_list:
            if pair[0] in hue_dict:
                hue_dict[pair[0]][0] += pair[1]
            else:
                hue_dict[pair[0]] = [pair[1], pair[2]]
        for key in hue_dict:
            if hue_dict[key][0] > 100:
                dominant_colours.add(hue_dict[key][1])
                last_value = hue_dict[key][0]
            else:
                break
        return list(dominant_colours)
    else:
        return []

def map_dominant_colours(dom_colours):
    """returns dictionary mapping dominant colours to their best match"""
    colour_map = nb.typed.Dict.empty(
        key_type=nb.types.UniTuple(nb.types.int64, 3),
        value_type=nb.types.UniTuple(nb.types.int64, 3)
    )
    if len(dom_colours) == 0:
        return colour_map
    if len(dom_colours)>len(GOOD_COLOURS):
            return colour_map
    distancematrix = [[colour_distance(g,d) for g in GOOD_COLOURS] for d in dom_colours]
    rows, columns = sco.linear_sum_assignment(distancematrix)
    for i in range(len(rows)):
        colour_map[dom_colours[rows[i]]] = GOOD_COLOURS[columns[i]]
    return colour_map

def colour_distance(c1, c2):
    """metric for determining closeness of colours"""
    return sum([(c1[i]-c2[i])**2 for i in range(3)])

def convert_to_dark(filename, oldpath, newname, newpath, verbose=False, dpi=300, skip=False):
    """Reads in Image, flips grey values and re-maps dominant colours"""
    if verbose:
        if filename.startswith("tmp"):
            print(f"Opening page {int(filename.split('.')[-2][3:])+1}...")
        else:
            print("Opening "+os.path.join(oldpath, filename)+"...")
    extension = filename.split('.')[-1]
    if extension not in ["png","jpg","jpeg","gif"]:
        if extension == "pdf":
            imgs = convert_from_path(os.path.join(oldpath, filename), dpi=dpi)
            for index, img in enumerate(imgs):
                img.save('tmp'+str(index)+'.png')
            if skip:
                return None
            img = Image.open('tmp0.png')
    else:
        img = Image.open(os.path.join(oldpath, filename))
    img = img.convert('RGB')
    if verbose:
        print("\tAnalysing colours...")
    img_array = np.array(img, dtype=np.int64)
    flat_img_array = img_array.reshape([img_array.size//3,3])
    colours = get_colours(flat_img_array)
    colour_map = map_dominant_colours(get_dominant_nongreys(colours))
    if verbose:
        print("\tRe-mapping colours...")
    new_image = rewrite(img_array, colour_map)
    if verbose:
        print("\tConverting array to image...")
    new_img = Image.fromarray(new_image)
    if verbose:
        print("\tOutputting converted image...")
    if extension in ["png","jpg","jpeg","gif"]:
        new_img.save(os.path.join(newpath, newname))
    else:
        new_img = new_img.convert('RGB')
        new_img.save(os.path.join(newpath, newname))
    return os.path.join(newpath, newname)

@nb.njit(parallel=True)
def rewrite(img_array, colour_map):
    """Returns array of image data with colours remapped with colour_map"""
    new_image = np.empty((img_array.shape[0], img_array.shape[1], 3),
                dtype=np.uint8)
    for y in nb.prange(img_array.shape[1]):
        for x in nb.prange(img_array.shape[0]):
            c = img_array[x,y]
            ct = (c[0], c[1], c[2])
            if ct in colour_map:
                new_image[x,y] = colour_map[ct]
            elif is_grey(ct) or len(colour_map)==0:
                new_image[x,y] = flipped(ct)
            else:
                new_image[x,y] = [0,0,0]
    return new_image

def merge_to_pdf(img_name_list, pdfname, verbose):
    """Takes a list of images and merges them into a single pdf file"""
    if verbose:
        print("Merging to pdf...")
    image_list = []
    for img_name in img_name_list[1:]:
        image_list.append(Image.open(img_name))
    Image.open(img_name_list[0]).save(pdfname, save_all=True, 
                                        append_images=image_list)
