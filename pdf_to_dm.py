import argparse
import os
import re
import traceback
from datetime import datetime
from convert_image import convert_to_dark, merge_to_pdf
from aux import print_time_diff, clean_up


def main(in_, out, dpi, verbose):
    if verbose:
        print("Preparing input data")
    try:
        starttime = datetime.now()
        pdfname = in_.split()[-1]
        pdfpath = '/'.join(in_.split()[0:-1])
        if pdfpath == '':
            pdfpath = '.'
        if out == "":
            out = in_[:-4] + "_dark.pdf"
        newname = out.split()[-1]
        newpath = '/'.join(out.split()[0:-1])
        if newpath == '':
            newpath = '.'
        os.mkdir('tmp')
        convert_to_dark(pdfname, pdfpath, 'tmp0.pdf', 'tmp', verbose, dpi, True)
        tmp_list = []
        for entry in os.listdir("."):
            if os.path.isfile(os.path.join(".", entry)):
                if re.match("tmp\d+\.png", entry) is not None:
                    tmp_list.append(convert_to_dark(entry, ".", entry, 'tmp', verbose))
        tmp_list.sort(key=(lambda x: int(x.split("/")[-1].split(".")[-2][3:])))
        merge_to_pdf(tmp_list, out, verbose)
    except Exception:
        traceback.print_exc()
    finally:
        clean_up()
        if verbose:
            print_time_diff(starttime, datetime.now())

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Convert a pdf to dark mode")
    parser.add_argument('--in', metavar='path', required=True, dest='in_',
                        help="the path to the white pdf")
    parser.add_argument('--out', metavar='path', required=False, default = "",
                        help="the new path for the converted pdf")
    parser.add_argument('--dpi', default=300, required=False,
                        help="the dpi of the output file, default: 300")
    parser.add_argument('-v', dest='verbose', action='store_true',
                        help="give updates on code progress")
    args = parser.parse_args()
    main(args.in_, args.out, args.dpi, args.verbose)
