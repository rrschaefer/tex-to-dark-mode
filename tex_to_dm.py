import argparse
import os
import traceback
from datetime import datetime
from convert_image import convert_to_dark
from convert_tex import copy_files, modify_files, compile_tex
from aux import print_time_diff, clean_up

def main(input_path, main, out_type, verbose):
    try:
        starttime = datetime.now()
        if input_path.endswith('/'):
            input_path = input_path[0:-1]
        output_path = input_path + "_dark"
        copy_files(input_path, output_path, verbose)
        found_bbl = modify_files(output_path, main, verbose)
    except Exception:
        traceback.print_exc()
    finally:
        clean_up()
        print_time_diff(starttime, datetime.now())
    if out_type == "pdf":
        compile_tex(output_path, main, verbose, found_bbl)
        if verbose:
            print_time_diff(starttime, datetime.now())

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Convert a paper's directory into a dark version")
    parser.add_argument('--path', metavar='path', required=True,
        help="the path to the white-based paper's directory")
    parser.add_argument('--main', metavar='filename', required=False, 
        default="main.tex", help="name of the main tex file without path")
    parser.add_argument('--tex', default='pdf', const='tex', dest='out_type',
        help="doesn't compile changed tex to to pdf", action='store_const')
    parser.add_argument('-v', dest='verbose', action='store_true',
        help="give updates on code progress")
    args = parser.parse_args()
    main(args.path, args.main, args.out_type, args.verbose)
