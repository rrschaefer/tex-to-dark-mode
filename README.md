## Tex to Dark Mode
This project is meant to turn the directory for a paper based on black print on white paper into white print on black paper.

For this purpose, it consists of the following source files:
- `convert_image.py` handles the functionality to convert png and pdf files into their dark mode versions
- `convert_tex.py` handles the functionality to convert tex files and their contained figures to dark mode and compile them to pdfs
and the following scripts:
- `python image_to_dm.py --in=input/path/file --out=output/path/file [--dpi=n] [-v]` converts the image under `input/path/file` into a dark-mode version of itself at `output/path/file`. If the file is a pdf, it will be converted with 300 dpi or the specified dpi.
- `python tex_to_dm.py --path=input/path [--main=filename.tex] [--tex] [-v]` converts the tex-document in the directory `input/path` into a dark-mode version of itself at `input/path_dark`, converting all its figures. It will then compile the .tex-file to pdf, unless --tex is used.
- `python pdf_to_dm.py --in=input/path/file --out=output/path/file [--dpi=n] [-v]` converts the pdf file under `input/path/file` into a dark-mode version of itself at `output/path/file`. It will be converted with 300 dpi or the specified dpi.
Generally, a plot's colours will be analysed and matched to a close colour that works well in dark mode. If there are too many colours present (for example in gradient plots or on PDF pages with too many plots of different colours), all colours in the plot are instead simply inverted.
The colours used are defined in `convert_image.py` in the variable `GOOD_COLOURS` and may be adapted for better usage.


Inspired by https://arxiv.org/pdf/2203.16546.pdf and using the colours mentioned in https://www.nature.com/articles/nmeth.1618.
