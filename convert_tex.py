import os
import shutil
import argparse
import re

from convert_image import convert_to_dark

def copy_files(input_path, output_path, verbose=False):
    """Copies the whole directory."""
    if verbose:
        print("Copying directory...")
    if output_path in os.listdir("."):
        shutil.rmtree(output_path)
    shutil.copytree(input_path, output_path)

def modify_files(path, main, verbose=False):
    """Looks through directories in path to find and modify main and figures"""
    if verbose:
        print("Looking through "+path+"...")
    found_bbl = False
    found_main = False
    figure_paths = []
    for entry in os.listdir(path):
        if found_main and found_bbl:
            break
        if os.path.isfile(os.path.join(path, entry)):
            if entry == main:
                figure_paths = modify_main_tex(os.path.join(path, entry),
                                                verbose)[0]
                found_main = True
            if entry.split(".")[-1] == "bbl":
                found_bbl = True
        if os.path.isdir(os.path.join(path, entry)):
            found_bbl = found_bbl or modify_files(os.path.join(path, entry),
                                                    main, verbose)
    for figure_path in figure_paths:
        *path_list, filename = figure_path.split("/")
        f_path = "/".join(path_list)
        convert_to_dark(filename, f_path, filename, f_path, verbose)
    return found_bbl

def modify_main_tex(filename, verbose=False, _doc_started=False):
    """Modifies the main.tex and finds referenced figures of the paper."""
    href = False
    xc = False
    doc_started = _doc_started
    here_path = "/".join(filename.split("/")[:-1])+"/"
    figure_paths = []
    if verbose:
        print("Modifying "+filename+"...")
    with open(filename, 'r') as main_old:
        with open(filename+"_tmp.tex", 'w') as main_new:
            for line in main_old.readlines():
                if (re.match(r"[^#]*\\usepackage\{[^#\}]*(hyperref|pub)[^#\}]*\}", line) is not None):
                    href = True
                if (re.match(r"[^#]*\\usepackage\{[^#\}]*(xcolor)[^#\}]*\}", line) is not None):
                    xc = True
                if (re.match(r"[^#]*\\begin\{document\}", line) is not None):
                    print("Doc Started")
                    main_new.write(r"\usepackage{pagecolor}"+"\n")
                    main_new.write(r"\pagecolor{black}"+"\n")
                    main_new.write(r"\color{white}"+"\n")
                    if href:
                        if not xc:
                            main_new.write(r"\usepackage{xcolor}"+"\n")
                        main_new.write(r"\definecolor{lightblue}{RGB}{97, 160, 255}"+"\n")
                        main_new.write(r"\hypersetup{allcolors=lightblue}"+"\n")
                    doc_started = True
                match_line = re.match(r"[^#]*\\(input|include|includegraphics)(\[[^#]*\])?\{(.*)\}", line)
                if match_line is not None:
                    if match_line[1] == "includegraphics":
                        if doc_started:
                            figure_name = match_line[3]
                            figure_paths.append(here_path + figure_name)
                            if verbose:
                                print("\tfound and saved figure " + match_line[3])
                    elif match_line[1] == "input" or match_line[1] =="include":
                        file_name = match_line[3]
                        if not file_name.endswith(".tex"):
                            file_name += ".tex"
                        return_result = modify_main_tex(here_path + file_name, verbose, doc_started)
                        xc = xc or return_result[2]
                        href = href or return_result[1]
                        figure_paths += return_result[0]
                main_new.write(line)
    os.rename(filename+"_tmp.tex", filename)
    return (figure_paths, href, xc)

def compile_tex(output_path, main, verbose=False, with_bbl=False):
    """compiles the tex-file at output_path/main to pdf via pdflatex, bibtex"""
    if verbose:
        print("Compiling Tex to PDF...")
    if with_bbl:
        os.system(f"cd {output_path}; pdflatex {main}; pdflatex {main}; cd ..")
    else:
        os.system(f"cd {output_path}; pdflatex {main};  bibtex {main.split('.')[0]}; pdflatex {main}; pdflatex {main}; cd ..")
