import os
import shutil
from datetime import datetime

def print_time_diff(starttime, endtime):
    """Prints the time difference in hours, minutes and seconds to console."""
    duration = (endtime - starttime).total_seconds()
    timestring = ""
    if duration >= 3600:
        h = int(duration // 3600)
        timestring += f"{h}h "
        duration %= 3600
    if duration >= 60:
        m = int(duration // 60)
        timestring += f"{m}min "
        duration %= 60
    timestring += f"{round(duration, 1)}s"
    print(f"This took {timestring}.")

def clean_up(verbose=False):
    """gets rid of tmp files and directories produced."""
    if verbose:
        print("Deleting temporary files...")
    for entry in os.listdir("."):
        if entry.startswith("tmp"):
            if os.path.isfile(os.path.join(".", entry)):
                os.remove(os.path.join(".", entry))
            elif os.path.isdir(os.path.join(".", entry)):
                shutil.rmtree(os.path.join(".", entry))

