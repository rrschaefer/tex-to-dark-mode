import argparse
import traceback
from datetime import datetime
from convert_image import convert_to_dark
from aux import print_time_diff, clean_up

def main(in_, out, dpi, verbose):
    if verbose:
        print("Preparing input data")
    try:
        starttime = datetime.now()
        filename = in_.split("/")[-1]
        oldpath = '/'.join(in_.split()[0:-1])
        if oldpath == '':
            oldpath = '.'
        if out == "":
            out = in_[:-4] + "_dark.pdf"
        newname = out.split()[-1]
        newpath = '/'.join(out.split()[0:-1])
        if newpath == '':
            newpath = '.'
        if dpi is None:
            convert_to_dark(filename, oldpath, newname, newpath, verbose)
        else:
            convert_to_dark(filename, oldpath, newname, newpath, verbose, dpi)
    except Exception:
        traceback.print_exc()
    finally:
        clean_up(verbose)
        if verbose:
            print_time_diff(starttime, datetime.now())

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Convert a figure into dark mode")
    parser.add_argument('--in', metavar='path', required=True, dest='in_',
                        help="the path to the white figure")
    parser.add_argument('--out', metavar='path', required=False, default="",
                        help="the new path for the converted figure")
    parser.add_argument('--dpi', metavar='number', required=False,
                        help="the dpi of the output file if converting a pdf")
    parser.add_argument('-v', dest='verbose', action='store_true',
                        help="give updates on code progress")
    args = parser.parse_args()
    main(args.in_, args.out, args.dpi, args.verbose)
